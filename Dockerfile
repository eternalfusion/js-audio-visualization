FROM node:18-alpine AS builder
WORKDIR /usr/app
COPY package*.json ./
COPY tsconfig*.json ./
RUN yarn install
COPY . ./
RUN yarn run build

# Setup nginx server for frontend
FROM nginx:stable-alpine AS nginx
COPY --from=builder /usr/app/dist/ /usr/share/nginx/html/
EXPOSE 80
CMD ["nginx", "-g", "daemon off;" ]
