import React, {FunctionComponent, useState} from 'react'
import Sketch from "react-p5";
import {useWindowSize} from "@react-hook/window-size";
import p5Types from "p5";
import {DrawUtils} from "../utils/utils";
import {Particle} from "../draw/Particle";
import {useSelector} from "react-redux";
// @ts-ignore
import {MeydaAnalyzer} from "meyda";

/*
* It's better to store variables which updates in loop separately from React state system due to rerender overhead
* */
const FFT_SIZE = 32
let prevEnergy = 1
let particles: Particle[] = []
let fps = 0
let prevModifiedX = 0
let prevModifiedY = 0

type VisualizerProps = {
    meydaAnalyzer: MeydaAnalyzer,
    dataArray: Uint8Array,
    analyser: AnalyserNode
}

const Visualizer: FunctionComponent<VisualizerProps> = ({ meydaAnalyzer, dataArray, analyser }) => {
    const playing = useSelector((state: any) => state.isPlaying)

    const [width, height] = useWindowSize()
    const [bg, setBg] = useState<p5Types.Image>()
    const [exofont, setExofont] = useState<p5Types.Font>()

    const preload = (p5: p5Types) => {
        p5.loadImage("/bg.jpg", img1 => setBg(img1.get()))
        setExofont(p5.loadFont("/Exo2-Bold.otf"))
    }

    const setup = (p5: p5Types, canvasParentRef: Element) => {
        p5.createCanvas(width, height, p5.WEBGL).parent(canvasParentRef);
        p5.background(24)
        p5.frameRate(144)
        p5.noStroke()
    };

    const draw = (p5: p5Types) => {
        p5.strokeWeight(0)
        p5.background(24)
        p5.imageMode(p5.CENTER)
        if (bg) {
            const centerX = p5.windowWidth / 2 - p5.mouseX
            const centerY = p5.windowHeight / 2 - p5.mouseY

            const modifiedX = prevModifiedX * 0.9 + p5.constrain(Math.sign(centerX) * p5.log(p5.abs(centerX !== 0 ? centerX : 1)) * 5, -200, 200) * 0.1
            const modifiedY = prevModifiedY * 0.9 + p5.constrain(Math.sign(centerY) * p5.log(p5.abs(centerY !== 0 ? centerY : 1)) * 5, -200, 200) * 0.1
            prevModifiedX = modifiedX
            prevModifiedY = modifiedY

            p5.image(bg!, modifiedX, modifiedY, bg.width / 2 * 1.2, bg.height / 2 * 1.2)
        }
        p5.fill(0, 0, 0, 50)

        p5.rect(- p5.windowWidth / 2, - p5.windowHeight / 2, p5.windowWidth, p5.windowHeight)

        p5.textFont(exofont!)

        p5.fill(255, 255, 255, 200)

        fps = (p5.frameCount % 10 == 1) ? Math.floor(p5.frameRate()) : fps

        p5.text(`FPS: ${fps}`, -p5.windowWidth/2+75, -p5.windowHeight/2 + 88/2 + p5.textAscent() / 2)
        drawVisualizer(p5)

    };
    const windowResized = (p5: p5Types) => {
        p5.resizeCanvas(p5.windowWidth, p5.windowHeight)
    };
    const drawVisualizer = (p5: p5Types) => {
        let offset = 0;
        p5.strokeWeight(1)
        const base_radius = 150

        const rms = 2 * Math.max(25, 25 + meydaAnalyzer ? meydaAnalyzer.get("rms") * 15 : 0)
        const spectralFlatness = meydaAnalyzer ? meydaAnalyzer.get("spectralFlatness") * 500 : 0

        analyser!.getByteFrequencyData(dataArray)

        offset = 200  / 100

        const filledOuterShape: { x: number; y: number; }[] = []

        DrawUtils.isolate(p5, (p5) => {
            p5.rotate( p5.millis() / 1000 *  p5.PI / 4)
            for (let i = 0; i < FFT_SIZE; i++) {
                const dist = p5.map(dataArray[i], 1, FFT_SIZE, 1, FFT_SIZE)
                let angle = p5.map((i) % FFT_SIZE, 0, FFT_SIZE, 0, Math.PI * 2)

                p5.fill(237, 103, 208,255)

                p5.rotate(angle);

                p5.rect(-20, 210, 40, (rms + dist) - 45);
            }
        })

        for (let i = 0; i < FFT_SIZE * 4; i++) {
            const noise = p5.constrain(p5.noise(i + offset), 0.1, 0.5) * p5.constrain(spectralFlatness, 1, 3) * (dataArray[i] / 20)

            let angle = p5.map((i + offset) % (FFT_SIZE * 4), 0, FFT_SIZE * 4, 0, Math.PI * 2)

            let x11 = ((!isNaN(noise) ? noise : 0) + base_radius + rms + 16) * Math.cos(angle)
            let y11 = ((!isNaN(noise) ? noise : 0) + base_radius + rms + 16) * Math.sin(angle)

            filledOuterShape.push({ x: x11, y: y11 })
        }

        p5.strokeWeight(0)
        p5.stroke(0, 0, 0, 255)
        p5.fill(0, 0, 0, 255)

        const songArtist = "Mili"
        const songName = "world.execute(me);"
        p5.textSize(p5.constrain(rms, 50, 55) / 1.5)
        p5.textSize(32)
        p5.textFont(exofont!)

        const energy = meydaAnalyzer.get("energy")
        prevEnergy = p5.map(prevEnergy * 0.9 + (p5.constrain(energy, 1, 4) * 0.1), 1, 1.3, 1, 1.3, true)

        DrawUtils.isolate(p5, (p5) => {
            //"#ffffff"
            p5.fill(34, 53, 80, 255)
            p5.scale(prevEnergy, prevEnergy,1)
            //polygon(p5,0,0,base_radius + rms, 90)
            p5.stroke(0)
            p5.ellipse(0, 0, (base_radius + rms) * 2.2, (base_radius + rms) * 2.2, 50);
            p5.fill(255, 255, 255, 200)
            p5.text(songArtist, -p5.textWidth(songArtist) / 2, -15)
            p5.text(songName, -p5.textWidth(songName) / 2, 25)
        })

        DrawUtils.isolate(p5, (p5) => {
            p5.scale(1.2, 1.2,1)
            p5.strokeWeight(1)
            p5.stroke(255, 255, 255)
            p5.fill(0,0,0,0);

            p5.beginShape();
            filledOuterShape.forEach((it, ind) => p5.vertex(it.x, it.y))
            p5.endShape(p5.CLOSE);
        })

        particles.push(new Particle(p5))

        for(var i = particles.length - 1; i >= 0; i--) {
            if(!particles[i].edges(p5.windowWidth, p5.windowHeight)) {
                particles[i].update(playing)
                particles[i].show()
            } else {
                particles.splice(i, 1)
            }
        }
    }

    return (
        <Sketch setup={setup} draw={draw} windowResized={windowResized} preload={preload}/>
    );
}
export default Visualizer;
