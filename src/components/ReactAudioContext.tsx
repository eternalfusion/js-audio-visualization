import { createContext } from "react";

const reactAudioContext = new AudioContext();
const context = createContext(reactAudioContext);

export default context;