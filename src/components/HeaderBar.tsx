import React, {FunctionComponent} from 'react'

import {Box, Button, Grid, IconButton, SxProps} from "@mui/material";
import {toggle} from "../reducers/playingReducer";
import {useDispatch, useSelector} from "react-redux";
import PlayArrowIcon from '@mui/icons-material/PlayArrow';
import PauseIcon from '@mui/icons-material/Pause';
import RepeatIcon from '@mui/icons-material/Repeat';

import {toggleRepeating} from "../reducers/repeatingReducer";

type HeaderBarProps = {}

const containerStyle: SxProps = {
    position: 'absolute',
    padding: '16px'
}

const HeaderBar: FunctionComponent<HeaderBarProps> = ({}) => {
    const playing = useSelector((state: any) => state.isPlaying)
    const repeating = useSelector((state: any) => state.isRepeating)
    const dispatch = useDispatch()

    return (
        <Grid sx={containerStyle} container justifyContent={"flex-end"}>
            <IconButton color="primary" onClick={() => dispatch(toggle(playing))}>
                { playing ? <PauseIcon sx={{ fontSize: 40 }} /> : <PlayArrowIcon sx={{ fontSize: 40 }} /> }
            </IconButton >
            <IconButton color="primary" onClick={() => dispatch(toggleRepeating(repeating))}>
                <RepeatIcon sx={{ fontSize: 40, opacity: repeating ? 1.0 : 0.3 }} />
            </IconButton >
        </Grid>
    );
}
export default HeaderBar;
