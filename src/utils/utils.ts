import p5Types from "p5";

export namespace DrawUtils {
    export function isolate(p5: p5Types, code: (p5: p5Types) => void): void {
        p5.push();
        code(p5);
        p5.pop();
    }

}
