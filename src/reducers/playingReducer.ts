import { createSlice } from '@reduxjs/toolkit'

export const playingSlice = createSlice({
    name: 'isPlaying',
    initialState: false,
    reducers: {
        toggle: (state) => !state,
    },
})

// Action creators are generated for each case reducer function
export const { toggle } = playingSlice.actions

export default playingSlice.reducer