import { createSlice } from '@reduxjs/toolkit'

export const repeatingSlice = createSlice({
    name: 'isRepeating',
    initialState: false,
    reducers: {
        toggleRepeating: (state) => !state,
    },
})

export const { toggleRepeating } = repeatingSlice.actions

export default repeatingSlice.reducer