import p5Types from "p5";

export const polygon = (p5: p5Types, x: number, y: number, radius: number, npoints: number) => {
    let angle = p5.TWO_PI / npoints;
    p5.beginShape();
    for (let a = 0; a < p5.TWO_PI; a += angle) {
        let sx = x + Math.cos(a) * radius;
        let sy = y + Math.sin(a) * radius;
        p5.vertex(sx, sy);
    }
    return p5.endShape(p5.CLOSE);

}