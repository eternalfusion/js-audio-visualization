import p5Types from "p5";


export class Particle{
    private _p5: p5Types
    private pos: p5Types.Vector;
    private vel: p5Types.Vector;
    private acc: any;
    private w: any;
    private color: any[];

    constructor(p5: p5Types) {
        this._p5 = p5
        // @ts-ignore
        this.pos = this._p5.constructor.Vector.random2D().mult(225)
        this.vel = this._p5.createVector(0,0)
        this.acc = this.pos.copy().mult(this._p5.random(0.0001, 0.00001))

        this.w = this._p5.random(3, 5)
        this.color = [this._p5.random(0,255), this._p5.random(0,255), this._p5.random(0,255)]
    }
    update(condition: boolean) {
        this.vel.add(this.acc)
        this.pos.add(this.vel)
        if(condition) {
            this.pos.add(this.vel)
            this.pos.add(this.vel)
            this.pos.add(this.vel)
        }
    }
    edges(width: number, height: number) {
        return this.pos.x < -width / 2 || this.pos.x > width / 2 || this.pos.y < -height / 2 || this.pos.y > height / 2;
    }
    show() {
        this._p5.noStroke()
        this._p5.fill(this.color)
        this._p5.ellipse(this.pos.x, this.pos.y, this.w)
    }
}