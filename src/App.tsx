import {useContext, useEffect, useRef, useState} from 'react'
import './App.css'
import Sketch from "react-p5";
import p5Types from "p5";
import {useWindowSize} from '@react-hook/window-size'
// @ts-ignore
import Meyda, {MeydaAnalyzer} from "meyda"
import ReactHowler from 'react-howler'
import HeaderBar from "./components/HeaderBar";
import {useSelector} from "react-redux";
import Visualizer from "./components/Visualizer";

let dataArray: Uint8Array;


function App() {
    const playing = useSelector((state: any) => state.isPlaying)
    const repeating = useSelector((state: any) => state.isRepeating)

    const audioEl = useRef(null)
    const [analyser, setAnalyser] = useState<AnalyserNode>()
    const [meydaAnalyzer, setMeydaAnalyzer] = useState<MeydaAnalyzer>()

    useEffect(() => {
        const analyserCreated = Howler.ctx.createAnalyser();
        setAnalyser(analyserCreated)

        Howler.masterGain.connect(analyserCreated);

        analyserCreated.connect(Howler.ctx.destination);

        dataArray = new Uint8Array(analyserCreated.frequencyBinCount)

        // @ts-ignore
        const gainNode = Howler._howls[0]._sounds[0]._node as GainNode
        const ma = Meyda.createMeydaAnalyzer({
            audioContext: Howler.ctx,
            source: gainNode,
            featureExtractors: ["rms", "spectralFlatness", "energy"],
        })
        setMeydaAnalyzer(ma)
        return () => {
            analyserCreated.disconnect()
            analyser?.disconnect()
        }
    }, [audioEl.current])



    return (
    <div className="App">
        <HeaderBar/>
        <Visualizer meydaAnalyzer={meydaAnalyzer} dataArray={dataArray} analyser={analyser!} />
        <ReactHowler
            preload={true}
            src='/world_execute_me.mp3'
            volume={0.2}
            playing={playing}
            ref={audioEl}
            loop={repeating}
        />

    </div>
  )
}

export default App
