import { configureStore } from '@reduxjs/toolkit'
import playingReducer from "./reducers/playingReducer";
import repeatingReducer from "./reducers/repeatingReducer";

const store = configureStore({
    reducer: {
        isPlaying: playingReducer,
        isRepeating: repeatingReducer,
    },
});

export default store;